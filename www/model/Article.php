<?php

namespace www\model;

class Article {
    public $id;
    public $title;
    public $description;
    public $content;
    public $ddate;
    public $tags;
    public $type;

    public function __construct($id, $title, $description, $content, $ddate, $tags, $type)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->content = $content;
        $this->ddate = $ddate;
        $this->tags = $tags;
        $this->type = $type;
    }
}
