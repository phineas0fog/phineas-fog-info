<?php
/**
 * Created by IntelliJ IDEA.
 * User: phineas
 * Date: 17/03/18
 * Time: 11:57
 */

class ImageUtils {
    public static function upload() {
        // $target_file = $target_dir . $targetName;
        // $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);


        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        $targetName = date('Y-m-d_H-i-s', time()) . "-" . $_SESSION['entId'] . "." . $fileType;
        $target_file = $target_dir . $targetName;
        $target_file = str_replace(" ", "_", $target_file);

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            if($fileType != 'jpg' AND $fileType != 'png') {
                throw new Exception("Le fichier n'est pas une image ou n'est pas au format jpg.");
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            throw new Exception("Le fichier existe déjà.");
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 10000000) {
            throw new Exception("Le fichier est trop gros.");
        }

        // Check if $uploadOk is set to 0 by an error
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            return $target_file;
        } else {
            throw new Exception("Le fichier n'a pas été envoyé.");
        }
    }
}