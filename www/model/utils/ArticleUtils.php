<?php

namespace www\model\utils;

use www\core\Logger;
use www\gateway\ArticleGateway;
use www\core\Validation;
use www\model\Article;
use \Exception;
use \PDOException;

class ArticleUtils
{
    private $gw;

    public function __construct()
    {
        $this->gw = new ArticleGateway();
    }

    public function getAllSumm() {
        return $this->gw->getAllSumm();
    }

    public function getById(int $id) {
        return $this->gw->getById($id);
    }

    public function add(String $jsonData) {
        $jsonObj = json_decode($jsonData);
        $art = new Article(0, $jsonObj->{'title'}, $jsonObj->{'desc'}, $jsonObj->{'content'}, time(), $jsonObj->{'tags'}, "article" );
        return $this->gw->add($art);
    }

    public function update($id, $jsonObj) {
        $content = $jsonObj->{'content'};
        return $this->gw->update($id, $content);
    }
}
