<?php

/**
 * Routes config
 * Here are all the routes declarations.
 * The first column is the route (url) and the second col is the controller.action
 * who must to be called.
 *
 * Example: '/' => 'VisitorController.call',
 * this line say "when the url is '/', call the method 'call' of the controller 'VisitorController'"
 */

return [
    'ROOT_FOLDER'         => 'controller',

    '/'                   => 'ArticlesController.all',
    '/articles'           => 'ArticlesController.all',
    '/articles/:id'       => 'ArticlesController.byId',

    '/cv'                 => 'PagesController.cv',
    '/real'               => 'PagesController.real',

    '/login'              => 'AdminController.login',
    '/verify'             => 'AdminController.verify',
    '/logout'             => 'AdminController.logout',

    '/upload'             => 'AdminController.upload'
];