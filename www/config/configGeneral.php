<?php
    /**
     * General config file
     *
     * ['view']['name'] -> location for each view
     */
    $conf['rep'] = __DIR__.'/../';

    $conf['basePage']          = 'template/BASE.php';

    $conf['views']          = 'view';

    $conf['view']['homeView']  = 'view/home.php';
    $conf['view']['login']     = 'view/login.php';
    $conf['view']['register']  = 'view/register.php';
    $conf['view']['error']     = 'view/error.php';

    $conf['view']['workInProgress']    = 'view/error/workInProgress.php';

    $conf['logFile'] = 'appLog.log';

    return $conf;