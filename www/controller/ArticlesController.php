<?php
/**
 * Created by PhpStorm.
 * User: phineas
 * Date: 19/02/18
 * Time: 13:48
 */

namespace www\controller;

use \www\controller\BaseController;
use www\core\exceptions\MethodNotAllowedException;
use \Exception;
use www\core\exceptions\NotFoundException;
use www\core\Validation;
use \www\model\utils\ArticleUtils;
use Parsedown;
use www\core\Logger;


class ArticlesController extends BaseController {
    private $utils;
    private $parser;

    public function __construct()
    {
        $this->utils = new ArticleUtils();
        $this->parser = new Parsedown();
    }

    public function all() {
        try {
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $params['articles'] = $this->utils->getAllSumm();

                if (isset($_SERVER['CONTENT_TYPE']) AND strtolower($_SERVER['CONTENT_TYPE']) == "application/json") {
                    header("Content-Type:application/json");
                    echo json_encode($params['articles']);
                    return;
                }

                $this->call('home', $params);
            } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $json = file_get_contents('php://input');

                Logger::info('NEW ART', $json);

                try {
                    if ($this->utils->add($json)) {
                        header("HTTP/1.1 201 Created");
                        echo "Article ajouté";
                    } else {
                        header("HTTP/1.1 500 Internal Server Error");
                        echo "Erreur d'insertion";
                    }
                } catch (Exception $e) {
                    echo "Error";
                }
            }
        } catch (Exception $e) {
            $_SESSION['error'] = $e->getMessage();
            $this->call();
        }
    }

    public function byId($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            try {
                $params['article'] = $this->utils->getById($id);
                // $params['article']->content = $this->parser->parse($params['article']->content);

                if (isset($_SERVER['CONTENT_TYPE']) AND strtolower($_SERVER['CONTENT_TYPE']) == "application/json") {
                    header("Content-Type:application/json");
                    echo json_encode($params['article']);
                    return;
                }

                $this->call('article', $params);
            } catch (Exception $e) {
                $_SESSION['error'] = $e->getMessage();
                $this->call();
            }
        } else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $json = file_get_contents('php://input');
            $jsonObj = json_decode($json);
            $id = $jsonObj->{'id'};
            $content = $jsonObj->{'content'};

            Logger::info('UPDATE ART', $id);

            try {
                if ($this->utils->update($id, $content)) {
                    header("HTTP/1.1 202 Accepted");
                    echo "Article modifié";
                } else {
                    header("HTTP/1.1 500 Internal Server Error");
                    echo "Erreur lors de la modification";
                }
            } catch (Exception $e) {
                echo "Error";
            }
        }
    }
}