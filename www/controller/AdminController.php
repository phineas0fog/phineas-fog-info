<?php
/**
 * Created by PhpStorm.
 * User: phineas
 * Date: 21/02/18
 * Time: 17:43
 */

namespace www\controller;


use www\core\Config;

class AdminController extends BaseController
{
    public function login() {
        $this->call('login');
    }

    public function verify() {
        $trueLogin = Config::getInstance()->get('adminLogin');
        $trueHash = Config::getInstance()->get('adminHash');
        if (password_verify($_POST['pass'], $trueHash) AND $_POST['login'] == $trueLogin) {
            $_SESSION['role'] = 'admin';
            setcookie('token', 42);
        } else {
            $_SESSION['error'] = "Mot de passe invalide";
            header('Location:' . Config::getInstance()->get('ROOT_URL') . "login");
        }
        header('Location:' . Config::getInstance()->get('ROOT_URL'));
    }

    public function logout() {
        $_SESSION['role'] = null;
        unset($_COOKIE["token"]);
        setcookie("token","0",time()-1);
        header('Location:' . Config::getInstance()->get('ROOT_URL'));
    }
}