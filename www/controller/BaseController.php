<?php

/**
 * @class HomeController
 *
 * ----------------------------------------
 * License AGPL custom : commercialization is prohibited
 * https://www.gnu.org/licenses/agpl-3.0.fr.html
 *
 * @author : Evrard Van Espen
 * Creation : December 2017
 */

namespace www\controller;

use www\core\Config;
use www\model\utils\AdvertUtils;
use www\model\utils\EntrepriseUtils;
use www\model\utils\MessageUtils;
use www\model\utils\NewsUtils;
use www\model\utils\ProductUtils;
use www\model\utils\ArticleUtils;
use \Exception;
use Parsedown;
use www\core\Logger;

class BaseController {
    public function call($view = 'home', $params = []): void {
        setcookie("ROOT_URL", Config::getInstance()->get("ROOT_URL"));

        $page = Config::getInstance()->get('views') . '/' . $view . '.php';

        include Config::getInstance()->get('basePage');
    }
}