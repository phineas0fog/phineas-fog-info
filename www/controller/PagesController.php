<?php
/**
 * Created by PhpStorm.
 * User: phineas
 * Date: 19/02/18
 * Time: 13:48
 */

namespace www\controller;

use \www\controller\BaseController;
use www\core\Config;
use www\core\exceptions\MethodNotAllowedException;
use \Exception;
use www\core\exceptions\NotFoundException;
use www\core\Validation;
use \www\model\utils\ArticleUtils;
use Parsedown;


class PagesController extends BaseController {

    private $parser;

    /**
     * PagesController constructor.
     * @param $parser
     */
    public function __construct()
    {
        $this->parser = new Parsedown();
    }


    public function cv() {
        $this->call('cv');
    }


    public function real() {
        $this->call('real');
    }
}