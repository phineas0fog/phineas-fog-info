<?php

    /**
     * class Config
     *
     * ----------------------------------------
     * License AGPL custom : commercialization is prohibited
     * https://www.gnu.org/licenses/agpl-3.0.fr.html
     * Authors : Yann Surzur & Evrard Van Espen
     * Creation : December 2017
     */


namespace www\core;

class Config
{
    private static $_instance;
    protected $configPath = 'config/configMaster.php';
    protected $settings = [];

    private function __construct()
    {
        $this->settings = require($this->configPath);
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new Config();
        return self::$_instance;
    }

    public function get(String $key)
    {
        return $this->settings[$key];
    }
}
