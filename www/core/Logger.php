<?php
/**
 * Created by PhpStorm.
 * User: phineas
 * Date: 01/02/18
 * Time: 21:06
 */

namespace www\core;

use www\core\Config;
use \Exception;

class Logger
{
    private static function log(String $level, String $context, String $message) {
        $date = date('d/m/Y:H:i:s', time());
        $context = strtoupper($context);

        $string = "$date - $level - $context - $message\n";

        $file = Config::getInstance()->get('logFile');
        $file = fopen($file, "a+") or die ("Failed to opening log file");

        fwrite($file, $string);
        fclose($file);
    }

    public static function info(String $context, String $message) {
        self::log('LOG_INFO', $context, $message);
    }
    public static function warn(String $context, String $message) {
        self::log('LOG_WARN', $context, $message);
    }
    public static function error(String $context, String $message) {
        self::log('LOG_ERR', $context, $message);
    }
}