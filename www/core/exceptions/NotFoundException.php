<?php
/**
 * Created by PhpStorm.
 * User: phineas
 * Date: 19/02/18
 * Time: 15:23
 */

namespace www\core\exceptions;


class NotFoundException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}