<?php
/**
 * Created by PhpStorm.
 * User: phineas
 * Date: 19/02/18
 * Time: 14:07
 */

namespace www\core\exceptions;

use \Exception;
use Throwable;

class MethodNotAllowedException extends Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}