<?php
    define('ROOT', __DIR__);
    require_once __DIR__ . '/vendor/autoload.php';
    require ROOT . '/Autoloader.php';

    www\Autoloader::register();

    use www\core\Router;

    if(empty($_SESSION))
        session_start();

    $router = Router::getInstance();
    $router->init();