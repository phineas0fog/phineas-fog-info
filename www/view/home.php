<?php include 'view/component/header.php' ?>

<noscript>
    <div class="mainContainer grid cols3Center" id="main">
        <div class="col2">

            <?php if (isset($params['articles'])): ?>
                <ul id="articleList">
                    <?php foreach ($params['articles'] as $art): ?>
                        <li>
                            <div class="article" id="<?= $art->id ?>">
                                <span class="linkJS"><a href="<?= $root ?>articles/<?= $art->id ?>"><h1><?= $art->title ?></h1></a><i><?= $art->ddate ?></i></span>
                                <pre><?= $art->description ?></pre>
                                <?php if (isset($_SESSION['role']) AND $_SESSION['role'] == 'admin'): ?>
                                    <a onclick="edit(<?= $art->id ?>)" class="adminLink">Éditer</a>
                                <?php endif; ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

        </div>
    </div>
</noscript>

<div class="mainContainer grid cols3Center" id="main">
    <div class="col2">

        <input type="text" class="search" required placeholder="Recherche" id="searchBar" onkeyup="search()" >

        <div>
            <ul id="artList"></ul>
        </div>

    </div>
</div>

<div class="mainContainer grid cols3Center hidden" id="article">
</div>