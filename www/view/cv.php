<div class="mainContainer grid cols3Center" id="cv">
    <div class="col2 cv">
        <h1>À propos de moi</h1>
        <h2>CV</h2>
        <h3>Formation</h3>
        <span><p><strong>DUT informatique</strong></p><i>2015-2018</i></span>
        <p>IUT de Clermont-Ferrand</p>
        <span><p><strong>Baccalauréat</strong></p><i>2015</i></span>
        <p>Section STI2D mention très bien</p>
        <br>
        <br>

        <h3>Expériences professionnelles</h3>
        <span><p>Bénévolat lors du festival du Blues en Loire</p><i>2015</i></span>
        <span><p>Musicien professionnel dans un groupe de blues</p><i>2015</i></span>
        <span><p>Stage de seconde dans une entreprise de services et réparations informatique</p><i>2014</i></span>
        <p><i>Durée: 1 semaine</i></p>
        <br>
        <span><p>Stage de troisième dans une entreprise d'infographie, découverte des principaux outils</p><i>2012</i></span>
        <p><i>Durée: 1 semaine</i></p>
        <br>
        <br>

        <h3>Compétences informatiques</h3>
        <h4>Langages de programmation</h4>
        <ul>
            <li><i>Arduino</i></li>
            <li><i>Bash</i></li>
            <li><i>C</i></li>
            <li><i>CSS</i></li>
            <li><i>HTML</i></li>
            <li><i>Java8</i></li>
            <li><i>PHP7</i></li>
            <li><i>Python</i></li>
            <li><i>Ruby</i></li>
        </ul>

        <h4>Méthodes de modélisation</h4>
        <ul>
            <li>Modèle entité - association</li>
            <li><i>UML</i></li>
        </ul>

        <h4>Logiciels et frameworks</h4>
        <ul>
            <li><i>Atom</i></li>
            <li><i>Bootstrap</i></li>
            <li><i>Docker</i></li>
            <li><i>Emacs</i></li>
            <li><i>IntelliJ</i></li>
            <li><i>PHPStorm</i></li>
            <li><i>VirtualBox</i></li>
            <li><i>VMWare</i></li>
            <li><i>WebStorm</i></li>
        </ul>

        <h4>Systèmes de gestion de bases de données</h4>
        <ul>
            <li><i>Elasticsearch</i></li>
            <li><i>MongoDB</i></li>
            <li><i>MySQL</i></li>
            <li><i>Oracle SQL</i></li>
            <li><i>Redis</i></li>
        </ul>

        <h4>Systèmes d'exploitations et logiciels serveurs</h4>
        <ul>
            <li><i>Linux, *NIX</i></li>
            <li>Administration de réseaux locaux (Ethernet, <i>WiFi</i>, <i>NAT-PAT</i>, routage)</li>
            <li><i>Apache</i></li>
            <li><i>FTP</i></li>
            <li><i>NGINX</i></li>
            <li><i>Postfix / Dovecot</i></li>
            <li>Déploiement de machines virtuelles</li>
            <li>Déploiement avec <i>Docker</i></li>
        </ul>

        <h3>Information complémentaires</h3>
        <h4>Langues étrangères</h4>
        <p>Anglais niveau courant</p>
    </div>
</div>