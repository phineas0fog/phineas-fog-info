<?php if (isset($_SESSION['error']) AND !is_null($_SESSION['error'])): ?>

    <div id="dom-target" style="display: none;">
        <?php echo htmlspecialchars($_SESSION['error']); ?>
    </div>

    <div class='errorNotifContainer'>
        <div class='errorNotif' id="errorId">
            <div class="closebtn" onclick="this.parentElement.classList.add('hidden')">&times;</div>
            <?php echo htmlspecialchars($_SESSION['error']); ?>
        </div>
    </div>


    <script type="text/javascript">
    setInterval(function () {
        document.getElementById('errorId').classList.add('hidden');
    }, 10000);
    var div = document.getElementById("dom-target");
    var data = div.textContent;
    notify(data);
    </script>
<?php endif;
unset($_SESSION['error']);

///////////////////////////////////// NOTIFICATION

if (isset($_SESSION['notif'])): ?>

    <div id="dom-target" style="display: none;">
        <?php echo htmlspecialchars($_SESSION['notif']); ?>
    </div>

    <div class='messageNotifContainer'>
        <div class='messageNotif' id="notifId">
            <div class="closebtn" onclick="this.parentElement.classList.add('hidden')">&times;</div>
            <?php echo htmlspecialchars($_SESSION['notif']); ?>
        </div>
    </div>


    <script type="text/javascript">
    setInterval(function () {
        document.getElementById('notifId').classList.add('hidden');
    }, 10000);
    var div = document.getElementById("dom-target");
    var data = div.textContent;
    notify(data);
    </script>
<?php endif;
unset($_SESSION['notif']) ;