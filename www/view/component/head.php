<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?= www\core\Config::getInstance()->get('title') ?></title>

<link href="<?= $root ?>dist/min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!--- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"> --->



<link rel="icon" type="image/png" href="<?= $root ?>favicon.ico" />
