<footer class="footer grid cols3Center" id="footer">
    <div class="col2 footContent">
        <div class="about">
            <strong>À propos du site</strong>
            <p>Ce site est distribué sous license <i><a target="_blank" href="https://creativecommons.org/licenses/by-nc-sa/2.0/fr/">Creative Commons CC BY-NC-SA</a></i></p>
            <p>Dépot <i>git</i> du site: <a href="<?= \www\core\Config::getInstance()->get('repo') ?>">/phineas0fog/phineas-fog-info</a></p>
            <p>Crédit photo : <a target="_blank" href="https://unsplash.com/photos/gIgciC_WKnY">Alex Gorham</a></p>
        </div>
        <div class="contact">
            <strong>Contact</strong>
            <div class="footContentData">
                <span><i class="fas fa-envelope"></i> &nbsp; <a href="mailto:e.vanespen@protonmail.com">e.vanespen@protonmail.com</a></span>
            </div>
        </div>
    </div>
</footer>