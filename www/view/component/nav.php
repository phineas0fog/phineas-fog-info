<?php if (isset($_SESSION['role']) AND $_SESSION['role'] == 'admin'): ?>
    <div class="navAdmin" id="navAdmin">
        <button class="btnLink" onclick="editor(0)">Nouvel article</button>
        <a href="<?= $root ?>logout">Déconnexion</a>
    </div>
<?php endif; ?>

<div class="nav col2">
    <a href="<?= $root ?>real" class="navLink">Réalisations</a>
    <a href="<?= $root ?>"><img src="<?= $root ?>dist/img/logo.png" alt="" class="logo"></a>
    <a href="<?= $root ?>cv" class="navLink">À propos de moi</a>
</div>
