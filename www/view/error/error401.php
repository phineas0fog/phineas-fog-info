<div class='main cols3'>
    <div class='error col-2'>
        <img src="public/img/forbidden.png" alt="">
        <h1>401, seuls les utilisateurs connectés peuvent faire ceci...</h1>
        <h2><i>Vous pouvez vous <a href="<?= $root ?>/login">connecter</a>, ou <a href="<?= $root ?>/register">créer un compte</a>, ou <a href='<?= $root ?>'>revenir à l'accueil</a></i></h2>
    </div>
</div>
