<div class='main cols3'>
    <div class='error col-2'>
        <img src="public/img/error-404.png" alt="">
        <h1>404, ce que vous cherchez n'est pas là...</h1>
        <h2><i>Vous pouvez <a href='<?= $root ?>'>revenir à l'accueil</a>, ou chercher sur <a target='_blank' href='http://duckduckgo.com'>duckduckgo</a> ;)</i></h2>
    </div>
</div>
