<div class="mainContainer grid cols3Center" id="main">
<progress value="0"></progress>
    <?php if (isset($params['article'])): ?>
            <div class="article col2">
                <span><h1><?= $params['article']->title ?></h1><i><?= $params['article']->ddate ?></i></span>
                <pre><?= $params['article']->content ?></pre>
            </div>
    <?php endif; ?>
</div>

<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script>
var winHeight = $(window).height(),
  docHeight = $(document).height();
  max = docHeight - winHeight;

$(progress).attr('max', max);

var value = $(window).scrollTop();
$(progress).attr('value', value);

$(document).on('scroll', function() {
  value = $(window).scrollTop();
  progressBar.attr('value', value);
});

$(document).on('ready', function() {
  var winHeight = $(window).height(),
      docHeight = $(document).height(),
      progressBar = $('progress'),
      max, value;

  /* Set the max scrollable area */
  max = docHeight - winHeight;
  progressBar.attr('max', max);

  $(document).on('scroll', function(){
     value = $(window).scrollTop();
     progressBar.attr('value', value);
  });
});
</script>