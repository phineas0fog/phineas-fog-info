<?php

namespace www\gateway;

use MongoDB\Driver\Query;
use www\core\Config;
use \PDO;

class Connection extends \PDO {
    private $stmt;

    private $host;
    private $base;
    private $user;
    private $pass;

    private $dsn;

    public function __construct()
    {
        $this->host = Config::getInstance()->get('db')['host'];
        $this->base = Config::getInstance()->get('db')['base'];
        $this->user = Config::getInstance()->get('db')['user'];
        $this->pass = Config::getInstance()->get('db')['pass'];

        $this->dsn = "mysql:host=" . $this->host . ";dbname=" . $this->base;

        parent::__construct($this->dsn, $this->user, $this->pass);
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * prepare and execute a query
     * @param query the query to prepare & execute
     * @param array $parameters
     * @return bool
     */
    public function executeQuery($query, array $parameters=[])
    {
        $this->stmt=parent::prepare($query);
        foreach($parameters as $name=>$value){
            $this->stmt->bindValue($name, $value[0], $value[1]);
        }
        return $this->stmt->execute();
    }

    /**
     * return the results of the previous executed query
     */
    public function getResult()
    {
        return $this->stmt->fetchall();
    }

    /**
     * get the PDO error
     */
    public function errorInfo()
    {
        return $this->stmt->errorInfo();
    }
}
