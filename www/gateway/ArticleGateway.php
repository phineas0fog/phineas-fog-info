<?php
/**
 * class UserGateway
 *
 * ----------------------------------------
 * License AGPL custom : commercialization is prohibited
 * https://www.gnu.org/licenses/agpl-3.0.fr.html
 * Authors : Yann Surzur & Evrard Van Espen
 * Creation : December 2017
 */

namespace www\gateway;

use www\core\Config;
use www\gateway\Connection;
use www\model\Article;
use \PDO;
use \Exception;
use \PDOException;

class ArticleGateway {
    private $conf;
    private $con;
    private $table;

    public function __construct()
    {
        $this->conf = Config::getInstance();
        $this->con = new Connection();
        $this->table = 'article';
    }

    public function getAllSumm() {
        $query = "SELECT id,title,description,ddate,tags,type FROM $this->table ORDER BY ddate DESC";
        try {
            $this->con->executeQuery($query);
            $sel = $this->con->getResult();
            $res = array();
            foreach ($sel as $row) {;
                $res[] = new Article($row['id'], $row['title'], $row['description'], NULL, $row['ddate'], $row['tags'], $row['type']);
            }

            if (sizeof($res) < 1)
                throw new Exception("Aucun article trouvé.");

            return $res;
        } catch (PDOException $e) {
            throw new Exception("Erreur lors de la lecture en base de données.");
        }
    }

    public function getById(int $id) {
        $query = "SELECT id,title,content,ddate,tags,type FROM $this->table WHERE id = :id";
        try {
            $this->con->executeQuery(
                $query, array(
                    ':id'  => array($id,PDO::PARAM_INT)
                )
            );
            $sel = $this->con->getResult();
            $res = array();
            foreach ($sel as $row) {;
                $res[] = new Article($row['id'], $row['title'], NULL, $row['content'], $row['ddate'], $row['tags'], $row['type']);
            }

            if (sizeof($res) < 1)
                throw new Exception("Identifiant inexistant.");

            return $res[0];
        } catch (PDOException $e) {
            throw new Exception("Erreur lors de la lecture en base de données.");
        }
    }

    public function add(Article $art) {
        $query = "INSERT INTO $this->table (title, description, content, ddate, tags, type) VALUES (:title, :description, :content, CURRENT_TIMESTAMP, :tags, :type)";
        try {
            $this->con->executeQuery(
                $query, array(
                    ':title'       => array($art->title,PDO::PARAM_STR),
                    ':description' => array($art->description,PDO::PARAM_STR),
                    ':content'     => array($art->content,PDO::PARAM_STR),
                    ':tags'        => array($art->tags,PDO::PARAM_STR),
                    ':type'        => array($art->type,PDO::PARAM_STR)
                )
            );

            return true;
        } catch (PDOException $e) {
            return false;
        }

    }

    public function update($id, $newContent) {
        $query = "UPDATE $this->table SET content = :content WHERE id = :id";
        try {
            $this->con->executeQuery(
                $query, array(
                    ':id'      => array($id,PDO::PARAM_STR),
                    ':content' => array($newContent,PDO::PARAM_STR)
                )
            );

            return true;
        } catch (PDOException $e) {
            return false;
        }

    }
}
