<?php $root = www\core\Config::getInstance()->get('ROOT_URL'); ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php include 'view/component/head.php' ?>
    </head>
    <body onbeforeunload="backHandler()">
    <?php //include 'view/component/header.php' ?>

        <!--      PAGE      -->
        <?php include $page ?>
        <!--      /PAGE     -->

    <?php include 'view/component/footer.php' ?>

    </body>

    <?php include 'view/component/error.php' ?>
    <script src="dist/app.js"></script>

    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/railscasts.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>

</html>
