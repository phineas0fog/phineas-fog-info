let ham = require('hammerjs');
import { toHtml } from "./vueController";
import { getArt } from "./resourcesController";

export function swipperInit () {
    let ul, li;
    ul = document.getElementById("artList");
    li = ul.getElementsByTagName('li');

    for (let i = 0; i < li.length; i++) {
        let item = li[i];
        let manager = new Hammer.Manager(item);
        let Swipe = new Hammer.Swipe();
        manager.add(Swipe);
        manager.on('swiperight', function() {
            let div = item.getElementsByTagName('span')[0].innerHTML;
            let reg = RegExp("<a onclick=\"display(.*)\">.*").exec(div);
            let id =reg[1].replace('(','').replace(')', '');
            toHtml(getArt(id));
        });

    }
}