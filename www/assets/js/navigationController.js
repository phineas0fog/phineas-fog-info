let mousetrap = require('mousetrap');
let $ = require('jquery');

export function upDown() {
    let ul = document.querySelector('#articleList');
    let artList = ul.getElementsByTagName('li');

    mousetrap.bind('down', function () {
        var next = $("#articleList  li.selected").removeClass("selected").next();
        if (next.length) {
            next.addClass("selected");
        } else {
            $("#articleList li").first().addClass("selected");
        }
    });
    mousetrap.bind('up', function () {
        var prev = $("#articleList  li.selected").removeClass("selected").prev();
        if (prev.length) {
            $("#articleList li").last().addClass("selected");
        } else {
            prev.addClass("selected");
        }
    });
    mousetrap.bind('return', function () {
        $("#articleList  li.selected").removeClass("selected").prev();
        
    })
}