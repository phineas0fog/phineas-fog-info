import { getCook } from './utils';
import { isMobile, slideIn, slideOut } from "./utils";
let mousetrap = require('mousetrap');
let ham = require('hammerjs');

let commonmark = require('commonmark');

export function toHtml (article) {
    window.history.pushState({}, null, getCook('ROOT_URL') + 'articles/' + article.id);

    let reader = new commonmark.Parser();
    let writer = new commonmark.HtmlRenderer();
    let parsed = reader.parse(article.content);

    article.content = writer.render(parsed);

    let  html = "";
    html += "<div class=\"article col2\">\n";
    html += "<span><h1>" + article.title + "</h1><i>" + article.ddate + "</i></span>\n";
    html += "<pre>" + article.content + "</pre>\n";
    html += "</div>\n";
    html += "<button onclick='back()' class='btn-back' id='back'><i class=\"fas fa-long-arrow-alt-left\"></i></button>";

    if (!isMobile()) {
        // slide in
        slideIn();
    } else {
        document.querySelector('#article').style.left = 0;
        let item = document.querySelector('#article');
        let manager = new Hammer.Manager(item);
        let Swipe = new Hammer.Swipe();
        manager.add(Swipe);
        manager.on('swipeleft', function() {
            back();
        });
    }


    document.querySelector('#main').classList.add('hidden');
    document.querySelector('#footer').classList.add('hidden');

    document.querySelector('#article').classList.remove('hidden');
    document.querySelector('#article').innerHTML = html;

    mousetrap.bind(['escape', 'backspace'], function () {
        back();
    });


    // window.scrollTo(0,0);
}

export function toHtmlSumm(data) {
    let $artList = document.querySelector('#artList');
    let html = '';

    data.forEach (function (el) {
        html += "<li>";
        html += "<div class='article' id=" + el.id + ">";
        html += "<span class=\"linkJS\"><a onclick=\"display(" + el.id + ")\"><h1>" + el.title + "</h1></a><i>" + el.ddate + "</i></span>";
        html += "<pre>" + el.description + "</pre>";
        if (getCook('token') == 42) {
            html += '<a onclick="editor(' + el.id + ')" class="adminLink">Éditer</a>';
        }
        html += "</div>";
        html += "</li>";
    });

    $artList.innerHTML = html;
}

export function back() {
    if (!isMobile()) {
        // slide out
        slideOut();
    } else {
        document.querySelector('#article').style.left = -100 + 'vw';
    }

    document.querySelector('#main').classList.remove('hidden');
    document.querySelector('#footer').classList.remove('hidden');

    // window.scrollTo(0, 0);

    let  prevUrl = getCook('prevUrl');
    let  rootUrl = getCook('ROOT_URL');

    if (prevUrl != null) {
        window.history.pushState({}, "", prevUrl);
    } else {
        window.history.pushState({}, "", rootUrl);
    }
}