import { getCook } from './utils';
import { toHtml, toHtmlSumm } from "./vueController";

export function getArt (id) {
    let rootUrl = getCook('ROOT_URL');

    let request = new XMLHttpRequest();

    let url;
    if (id != 0) {
        url = rootUrl + 'articles/' + id;
    } else {
        url = rootUrl;
    }

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let response = JSON.parse(this.responseText);
            if (id != 0) {
                toHtml(response);
            } else {
                toHtmlSumm(response);
            }
        }
    };
    request.open("GET", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.send();

    // document.cookie = "prevUrl=" + window.location;
    // window.history.pushState({}, null, "articles/" + id);
}