export function notify(type, message) {
    let typeClass;
    if (type === 'error') {
        typeClass = 'errorNotif';
    } else {
        typeClass = 'messageNotif';
    }

    let $notifContainer = document.createElement('div');
    $notifContainer.classList.add('notifContainer');
    $notifContainer.classList.add(typeClass);
    $notifContainer.innerHTML = message;

    document.querySelector('#main').appendChild($notifContainer);

    setTimeout(function () {
        $notifContainer.remove();
    }, 5000);
}