import { getCook } from "./utils";

require('simplemde/dist/simplemde.min.css');
let smde = require('simplemde');
import { post, put } from "./utils";

export function editor(id) {
    if (!document.querySelector('#editor')) {

        let privateEditor = function (initialValue) {
            let $main = document.querySelector('#main');
            let $editor = document.createElement('div');
            $editor.classList.add('adminModule');
            $editor.id = 'editor';
            $editor.classList.add('hidden');

            let $tArea = document.createElement('textarea');
            $tArea.id = 'tArea';
            $tArea.required = true;
            $tArea.style.minHeight = '20vh';

            $editor.appendChild($tArea);

            $main.appendChild($editor);

            let simplemde = new smde({
                autofocus: true,
                autosave: {
                    enabled: false,
                    uniqueId: "MyUniqueID",
                    delay: 1000,
                },
                blockStyles: {
                    bold: "**",
                    italic: "*"
                },
                element: $tArea,
                forceSync: true,
                indentWithTabs: false,
                initialValue: initialValue,
                insertTexts: {
                    horizontalRule: ["", "\n\n-----\n\n"],
                    image: ["![](http://", ")"],
                    link: ["[", "](http://)"],
                    table: ["", "\n\n| Column 1 | Column 2 | Column 3 |\n| -------- | -------- | -------- |\n| Text     | Text      | Text     |\n\n"],
                },
                lineWrapping: false,
                parsingConfig: {
                    allowAtxHeaderWithoutSpace: true,
                    strikethrough: false,
                    underscoresBreakWords: true,
                },
                promptURLs: true,
                renderingConfig: {
                    singleLineBreaks: false,
                    codeSyntaxHighlighting: true,
                },
                showIcons: ["code", "table"],
                spellChecker: false,
                status: ["autosave", "lines", "words", "cursor"], // Optional usage
                status: ["autosave", "lines", "words", "cursor", {
                    className: "keystrokes",
                    defaultValue: function(el) {
                        this.keystrokes = 0;
                        el.innerHTML = "0 Keystrokes";
                    },
                    onUpdate: function(el) {
                        el.innerHTML = ++this.keystrokes + " Keystrokes";
                    }
                }], // Another optional usage, with a custom status bar item that counts keystrokes
                styleSelectedText: false,
                tabSize: 4,
                toolbar: ['link', 'image', 'table', 'preview', 'side-by-side', 'fullscreen', '|',
                    {
                        name: "custom",
                        action: function customFunction(editor){
                            if (initialValue) {
                                send(simplemde.value(), 'PUT');
                            } else {
                                send(simplemde.value(), 'POST');
                            }
                        },
                        className: "fa fa-rocket",
                        title: "Envoyer",
                    },
                ]
            });

            $editor.classList.remove('hidden');
        };

        let send = function (content, method) {
            console.log(method);

            let art = content.split(/\+\+\+\+\+/);

            let data = {
                "title": art[0],
                "desc": art[1],
                "content": art[2],
                "tags": art[3]
            };

            // let data = {
            //     "title": document.querySelector('#title').value,
            //     "desc": document.querySelector('#desc').value,
            //     "content": content,
            //     "tags": document.querySelector('#tags').value
            // };

            if (method === 'POST') {
                post(data);
            } else if (method === 'PUT') {
                put(id, data);
            }
        };

        if (id != 0) {
            let rootUrl = getCook('ROOT_URL');
            let request = new XMLHttpRequest();
            let url = rootUrl + 'articles/' + id;
            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    let response = JSON.parse(this.responseText);
                    privateEditor(response.content);
                }
            };
            request.open("GET", url, true);
            request.setRequestHeader("Content-Type", "application/json");
            request.send();
        } else {
            privateEditor('');
        }

    } else {
        //document.querySelector('#editor').classList.toggle('hidden');
        document.querySelector('#editor').remove();
    }
}