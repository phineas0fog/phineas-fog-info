import { notify } from "./notifier";

export function isMobile () {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        return true;
    }
}

export function getCook (cookiename) {
    let cookiestring=RegExp(""+cookiename+"[^;]+").exec(document.cookie);
    return decodeURIComponent(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./,"") : "");
}

export function slideOut () {
    let el = document.querySelector('#article');
    let pos = 0;
    let id = setInterval(frame, 5);
    function frame() {
        if (pos == -100) {
            clearInterval(id);
            el.classList.add('hidden');
            el.innerHTML = "";
        } else {
            pos--;
            el.style.left = pos + 'vw';
        }
    }
}

export function slideIn () {
    let el = document.querySelector('#article');
    let pos = -100;
    let id = setInterval(frame, 5);
    function frame() {
        if (pos == 0) {
            clearInterval(id);
        } else {
            pos++;
            el.style.left = pos + 'vw';
        }
    }
}

export function search () { // from w3school
    let input, filter, ul, li, a, i;
    input = document.getElementById('searchBar');
    filter = input.value.toUpperCase();
    ul = document.getElementById("artList");
    li = ul.getElementsByTagName('li');

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

export function post(data) {
    let req = new XMLHttpRequest();
    req.open('POST', getCook('ROOT_URL') + 'articles/', true);
    req.setRequestHeader("Content-type", "application/json");
    req.onreadystatechange = function() {
        if(req.status == 201) {
            if (req.readyState == 4) {
                notify('msg', 'Article ajouté');
                if (document.querySelector('#editor')) {
                    document.querySelector('#editor').remove();
                }
            }
        } else {
            notify('error', "Problème d'ajout de l'article");
        }
    };
    req.send(JSON.stringify(data));
}

export function put(id, data) {
    let req = new XMLHttpRequest();
    req.open('PUT', getCook('ROOT_URL') + 'articles/' + id, true);
    req.setRequestHeader("Content-type", "application/json");
    req.onreadystatechange = function() {
        if(req.status == 202) {
            if (req.readyState == 4) {
                notify('msg', 'Article modifié');
                if (document.querySelector('#editor')) {
                    document.querySelector('#editor').remove();
                }
            }
        } else {
            notify('error', "Problème de modification de l'article");
        }
    };
    data = {
        id: id,
        content: data
    };
    req.send(JSON.stringify(data));
}
