import { search } from './utils';
import { getArt } from './resourcesController';
import { toHtml, back } from './vueController';
import { editor } from './editor';
import { swipperInit } from "./swipper";

global.display = function (id) {
    getArt(id);
    document.getElementsByTagName('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
    });
};
//
// global.search = function () {
//      return search();
// };
//
// global.back = function () {
//     console.log('back');
//     back();
// };
//
swipperInit();


global.search = function () {
    return search();
};


global.editor = function (id) {
    return editor(id);
};

window.onload = function () {
    getArt(0);
};