let gulp = require('gulp'),
    compass = require('gulp-compass'),
    concat = require('gulp-concat-css'),
    path = require('path'),
    postcss = require('gulp-postcss'),
    imagemin = require('gulp-imagemin'),
    exec = require('child_process').exec,
    livereload = require('gulp-livereload'),
    conventionalChangelog = require('gulp-conventional-changelog');

gulp.task('styles', function () {
    let processors = [
        require('postcss-instagram'),
        require('rucksack-css'),
        require('autoprefixer')({browsers: ['last 2 version']}),
        require('css-mqpacker'),
        require('postcss-merge-idents'),
        require('cssnano')({
	    preset: 'advanced',
        })
    ];
    return gulp.src(path.resolve('./www/assets/styles/*.scss'))
        .pipe(compass({
	    css: './www/assets/css',
	    sass: './www/assets/styles'
        }))
        .pipe(concat('min.css'))
        .pipe(postcss(processors))
        .pipe(gulp.dest('./www/dist/'))
        .pipe(livereload())
});

gulp.task('imagemin', function () {
    gulp.src('www/assets/img/*')
		.pipe(imagemin([
            imagemin.optipng({optimizationLevel: 5}),
        ]))
		.pipe(gulp.dest('www/dist/img'))
});

gulp.task('DEPLOY', function(cb) {
    exec('./deploy.sh', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

gulp.task('changelog', function () {
    return gulp.src('CHANGELOG.md', {
	buffer: false
    })
	.pipe(conventionalChangelog({
	    preset: 'angular'
	}))
	.pipe(gulp.dest('./'));
});

///////////////////////////////////////////////////////////////////////////////////

gulp.task('watch', function () {
    gulp.watch('www/assets/styles/*.scss', ['styles']).on('change', function (event) {
	console.log(event.path + ' updated');
    });
    gulp.watch(['www/dist/min.css', 'www/dist/app.js']).on('change', function() {
        livereload({ start: true });
        livereload.listen();
    })
});

gulp.task('default', ['styles'], function () {});
