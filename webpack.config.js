const path = require('path'),
    dev = process.env.NODE_ENV,
    UglifyJSPlugin = require('uglifyjs-webpack-plugin');


let config = {
    entry: './www/assets/js/app.js',
    output: {
        path: path.resolve('./www/dist'),
        filename: 'app.js'
    },
    devtool: dev ? "cheap-module-eval-source-map" : "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.coffee$/,
                use: [ 'coffee-loader' ]
            }
        ]
    },
    plugins: [
    ]
};

if (!dev) {
    config.plugins.push(new UglifyJSPlugin());
}

module.exports = config;